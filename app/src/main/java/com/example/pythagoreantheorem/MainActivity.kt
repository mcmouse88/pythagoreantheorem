package com.example.pythagoreantheorem

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.pythagoreantheorem.databinding.ActivityMainBinding
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    fun onClickResult(view: View) {
        if (!isFieldEmpty()) {
            val result = getText(R.string.result_info).toString() + getResult()
            binding.viewResult.text = result
        }
    }

    private fun isFieldEmpty():Boolean {
        binding.apply { // Получить доступ ко всем итемам активити, чтобы обращаться к ним напрямую
            if (editA.text.isNullOrEmpty()) editA.error = R.string.error_message.toString()
            if (editB.text.isNullOrEmpty()) editB.error = R.string.error_message.toString()
            return editA.text.isNullOrEmpty() || editB.text.isNullOrEmpty()
        }
    }

    private fun getResult(): String {
        val a: Double
        val b: Double
        binding.apply {
            a = editA.text.toString().toDouble()
            b = editB.text.toString().toDouble()
        }
        return String.format("%.2f", sqrt((a.pow(2) + b.pow(2))))
    }
}